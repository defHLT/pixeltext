package com.mlatu.pixeltest;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by ice on 11/13/14.
 */
public class DrawerBaseAdapter extends BaseAdapter {
    private final String[] mColorNames;
    private final int[] mColorVals;

    public DrawerBaseAdapter(Context ctx) {
        mColorNames = ctx.getResources().getStringArray(R.array.colornames);
        mColorVals = ctx.getResources().getIntArray(R.array.colorvals);
    }


    @Override
    public int getCount() {
        return mColorNames.length + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout f = (FrameLayout) inflater.inflate(R.layout.drawer_list_item, null);
        TextView tvLabel = (TextView) f.findViewById(R.id.text1);
        if (i == 0) {
            tvLabel.setText(viewGroup.getContext().getString(R.string.menu_label_help_screen));
        } else {
            tvLabel.setText(mColorNames[i - 1]);
            tvLabel.setTextColor(mColorVals[i - 1]);
        }
        tvLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager vp = (ViewPager) (viewGroup.getRootView()).findViewById(R.id.pager);
                vp.setCurrentItem(i);
            }
        });
        return f;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}

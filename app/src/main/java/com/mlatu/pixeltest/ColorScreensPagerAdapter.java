package com.mlatu.pixeltest;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class ColorScreensPagerAdapter extends FragmentStatePagerAdapter {
    private final int[] mColorsArray;

    public ColorScreensPagerAdapter(FragmentManager fm, Context context)  {
        super(fm);
        mColorsArray = context.getResources().getIntArray(R.array.colorvals);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment f;
        if (i == 0) {
            f = new HelpScreenFragment();
        } else {
            Bundle args = new Bundle();
            Fragment fragment = new ColorFragment();
            fragment.setArguments(args);
            args.putInt(ColorFragment.ARG_COLOR, mColorsArray[i - 1]);
            f = fragment;
        }
        return f;
    }

    @Override
    public int getCount() {
        return mColorsArray.length + 1;
    }

}
